create table Links(
    id serial PRIMARY KEY,
    url VARCHAR(500) NOT NULL,
    was_fetched BOOlEAN DEFAULT False
);


create table Articles(
    id serial PRIMARY KEY,
	title VARCHAR(500) NOT NULL,
	content TEXT NOT NULL,
	published_at TIMESTAMP NOT NULL,
    url VARCHAR(500) NOT NULL
);


DROP TABLE IF EXISTS ArticlesData;
CREATE EXTERNAL TABLE ArticlesData (
  authors ARRAY<STRING>,
  title STRING,
  content STRING,
  labels ARRAY<STRING>,
  published_at TIMESTAMP,
  url STRING
)
ROW FORMAT SERDE 'org.apache.hive.hcatalog.data.JsonSerDe'
LOCATION '/home/data';


SELECT count(*)
FROM bigdatalabs.articlesdata
WHERE content LIKE concat('%', 'Belarus', '%') ;


DROP TABLE IF EXISTS BelarussianUrls;
CREATE TABLE BelarussianUrls as
SELECT url, count(1) as belarus_word_count
FROM ArticlesData
LITERAL VIEW explide(split(lower(content), ",|!|\\.|\\?|'|`|\"|\\(|\\)| ")) a as word
where word == "belarus"
group by url, word;


DROP TABLE IF EXISTS BelarussianNews;
CREATE TABLE BelarussianNews as
SELECT news.authors, news.title, news.content, news.labels, news.published_at, news.url, bu.belarus_word_count
FROM bigdatalabs.articlesdata news
JOIN BelarussianUrls bu
ON news.utl = bu.url;


SELECT to_char(published_at, 'YYYY') as yyyy, count(*)
from BelarussianUrls
GROIP BY yyyy
ORDER BY yyyy;


SELECT sum(belarus_word_count) AS total_belarus_word_count
FROM BelarussianNews;

SELECT count(*) AS number_of_news, "All" AS kind_of_news FROM bigdatalabs.articlesdata
UNION ALL
SELECT count(*) AS number_of_news "Belarus" AS kind_of_news FROM BelarussianNews;
