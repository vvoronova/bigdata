import json
import string
import threading
from contextlib import closing
from multiprocessing import Process
from random import randint

import psycopg2
from fp.fp import FreeProxy
from lxml import html
from requests import Session

from .constants import *


def parse_article(tree, url):
    item = {
        'authors': tree.xpath(AUTHOR_SELECTOR),
        'title': str(tree.xpath(HEADLINE_SELECTOR)[0]),
        'content': ''.join(tree.xpath(CONTENT_SELECTOR)),
        'labels': tree.xpath(LABEL_SELECTOR),
        'published_at': str(tree.xpath(PUBLISHEDAT_SELECTOR)[0]),
        'url': url
    }
    return item


def fetch_items_task():
    with closing(psycopg2.connect(host="localhost", user="postgres", password="postgres")) as conn:
        with conn.cursor() as cursor:
            while True:
                cursor.execute(
                    '''select id, url, day, month, year from Links where was_fetched=False LIMIT 3 offset %s''',
                    (randint(1, 1000),))
                items = cursor.fetchall()
                try:
                    proxy = FreeProxy().get()
                    session = Session()
                    session.proxies = dict(http=proxy, https=proxy)

                    for id, url, day, month, year in items:
                        try:
                            response = session.get(url, cookies=cookies, headers=headers)
                            tree = html.fromstring(response.content)
                            item = parse_article(tree, url)

                            file_name = f'{day}_{month}_{year}_{item["title"]}'
                            remove_punctuation = str.maketrans('', '', string.punctuation)
                            file_name = file_name.translate(remove_punctuation)
                            file_name = file_name.replace(" ", "_")

                            with open(f'articles/{file_name}.json', 'w') as fp:
                                json.dump(item, fp)

                            cursor.execute('''UPDATE Links SET was_fetched = True WHERE id=%s;''', (id, ))
                            conn.commit()
                        except Exception:
                            break
                except Exception:
                    pass


def fetch_func():
    threads = []
    for i in range(8):
        my_thread = threading.Thread(target=fetch_items_task, args=(i,))
        my_thread.start()
        threads.append(my_thread)

    [item.join() for item in threads]


if __name__ == '__main__':
    for i in range(8):
        p = Process(target=fetch_func, args=())
        p.start()
