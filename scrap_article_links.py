
from contextlib import closing
from random import choice

import psycopg2
from bs4 import BeautifulSoup
from requests import Session

mons = {
    'jan': 31,
    'feb': 28,
    'mar': 31,
    'apr': 30,
    'may': 31,
    'jun': 30,
    'jul': 31,
    'aug': 31,
    'sep': 30,
    'oct': 31,
    'nov': 30,
    'dec': 31
}
years = [2020, 2019, 2018, 2017, 2016, 2015, 2014, 2013, 2012, 2011, 2010, 2009]
proxies = [

]

sessions = []
for proxy in proxies:
    session = Session()
    session.proxies = dict(
        http=proxy,
        https=proxy,
    )
    sessions.append(session)

with closing(psycopg2.connect(host="localhost", user="postgres", password="postgres")) as conn:
    with conn.cursor() as cursor:
        for day, mon in mons.items():
            for year in years:
                response = choice(sessions).get(f'https://www.theguardian.com/theguardian/mainsection/international/{year}/{mon}/{day}/all')

                soup = BeautifulSoup(response.content)

                q = {item['href'] for item in soup.findAll('a', attrs={'data-link-name': 'article'}, href=True)}

                for item in q:
                    cursor.execute('''INSERT INTO Links (url, day, month, year) VALUES (%s,%s,%s, %s)''', (item, day, mon, year))
                conn.commit()
