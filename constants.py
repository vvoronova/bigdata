HEADLINE_SELECTOR = '//*[contains(@itemprop,"headline")]//text()'
CONTENT_SELECTOR = '//*[contains(@class,"content__article-body")]//p//text()'
STANDFIRST_SELECTOR = '//*[contains(@class,"content__standfirst")]//p//text()'
AUTHOR_SELECTOR = '//*[contains(@rel,"author")]//*/text()'
LABEL_SELECTOR = '//*[contains(@class,"label__link-wrapper")]//text()'
PUBLISHEDAT_SELECTOR = '//*[contains(@class,"content__dateline-wpd")]//@datetime'


cookies = {
    'privacy-policy': '1,XXXXXXXXXXXXXXXXXXXXXX'
}


headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
